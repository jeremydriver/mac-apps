# Mac Apps
A nicely organised collection of mac apps I use.

---

## Menu Bar Apps

### [Dozer](https://dozermac.com/) 
> _Hide menu bar icons on macOS_

Really nice and simple app to hide other menubar icons from view. Toggleable by clicking icon or with shortcut key.<br>
I used to use [Vanilla](https://matthewpalmer.net/vanilla/), but dozer is less buggy and feels faster.


### [Clipy](https://clipy-app.com/)
> _macOS snippet app_

Keeps long history of the clipboard, and provides place to save and paste snippets.
(Presumably) a fork of ClipMenu, which I used to use.

### [Spectacle](https://www.spectacleapp.com/)
> Move and resize windows with ease.<br>
> Window control with simple and customizable keyboard shortcuts

For controlling location and sizing of windows.<br> 
I honestly prefer this to the windows native screen-moving shortcuts.

### [Tomighty](http://www.tomighty.org/)
> A free desktop timer for the Pomodoro Technique®

Pretty simple, just a nice and simple Pomodoro timer.


### SpotMenu

---


## Other Apps

### Übersicht
Rainmeter-like desktop apps.

### uBar 4
Taskbar replacement.

### Rocket
Emoji picker.

### Contraste
> Contraste is a simple app for checking the accessibility of text against the Web Content Accessibility Guidelines (WCAG).

### Gapplin 
SVG viewer (way better than Preview is for SVGs).

### Kap
Screen recorder.

### iTerm
Terminal.


# 2022
- kap - https://getkap.co/
- AltTab - https://alt-tab-macos.netlify.app/
- codekit
- Clipy (clipboard)
- Pika (colour picker) - https://superhighfives.com/pika
- Ractangle - https://rectangleapp.com/
- captin - caps lock notifier
- Dozer - hide status bar icons - https://github.com/Mortennn/Dozer
- Gapplin - display SVGs
- OminDiskSweeper - see storage usage
- SpotMenu - display spotify info in status bar
- iterm2 colour: Monokai Remastered https://iterm2colorschemes.com/


# 2023
- Maccy (clipboard) https://maccy.app/ (homebrew)
	- shortcut: cmd+opt+v
	- Paste automatically
	- Menu size: 20
	- Show menu icon, don't show recent copy, show search field, don't show title, don't show footer
- kap - https://getkap.co/
- Pika (colour picker) - https://superhighfives.com/pika
	- Hide pika while picking, launch at login, menu bar only
	- color format: sRGB
	- shortcut: ctrl+shift+`
- Rectangle - https://rectangleapp.com/
- captin - caps lock notifier
- Dozer - hide status bar icons - (homebrew) - https://github.com/Mortennn/Dozer
- Gapplin - display SVGs
- iterm2 colour: Monokai Remastered https://iterm2colorschemes.com/
- ack - brew install ack
- ncdu
- dev env: https://getgrav.org/blog/macos-ventura-apache-multiple-php-versions
- zsh profile, vimrc, inputrc, etc: https://gist.github.com/JezDriver/03d2171b7ee4686a6501305343f7f625
- git open: https://github.com/paulirish/git-open